<?php

use Bitrix\Main\Application,
    Bitrix\Main\ModuleManager,
    Bitrix\Main\Localization\Loc,
    Bitrix\Main\Loader;

IncludeModuleLangFile(__FILE__);

class project_discount extends CModule {

    public $MODULE_ID = 'project.discount';

    function __construct() {
        $arModuleVersion = array();

        include(__DIR__ . '/version.php');

        if (is_array($arModuleVersion) && array_key_exists('VERSION', $arModuleVersion)) {
            $this->MODULE_VERSION = $arModuleVersion['VERSION'];
            $this->MODULE_VERSION_DATE = $arModuleVersion['VERSION_DATE'];
        }

        $this->MODULE_NAME = Loc::getMessage('PROJECT_DISCOUNT_NAME');
        $this->MODULE_DESCRIPTION = Loc::getMessage('PROJECT_DISCOUNT_DESCRIPTION');
        $this->PARTNER_NAME = Loc::getMessage('PROJECT_DISCOUNT_PARTNER_NAME');
        $this->PARTNER_URI = '';
    }

    public function DoInstall() {
        ModuleManager::registerModule($this->MODULE_ID);
        Loader::includeModule($this->MODULE_ID);
        $this->InstallEvent();
    }

    public function DoUninstall() {
        Loader::includeModule($this->MODULE_ID);
        $this->UnInstallEvent();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->registerEventHandler('main', 'OnAfterUserUpdate', $this->MODULE_ID, '\Project\Discount\Event\User', 'OnAfterUserUpdate');
    }

    public function UnInstallEvent() {
        $eventManager = Bitrix\Main\EventManager::getInstance();
        $eventManager->unRegisterEventHandler('main', 'OnAfterUserUpdate', $this->MODULE_ID, '\Project\Discount\Event\User', 'OnAfterUserUpdate');
    }

}
