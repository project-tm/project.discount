<?

namespace Project\Discount\Event;

use CUser,
    Project\Discount\Config;

class User {

    public static function OnAfterUserUpdate(&$arFields) {
        if(empty($arFields['RESULT'])) {
            return;
        }
        $groups = array();
        $res = CUser::GetUserGroupList($arFields['ID']);
        while ($arGroup = $res->Fetch()) {
            $groups[$arGroup['GROUP_ID']] = $arGroup['GROUP_ID'];
        }
        $new = $groups;
        unset($new[Config::GROUP_1], $new[Config::GROUP_3], $new[Config::GROUP_5]);

        $arUser = CUser::getById($arFields['ID'])->Fetch();
        switch (true) {
            case $arUser['UF_BALL'] < 1000:
                break;
            case $arUser['UF_BALL'] >= 1000 and $arUser['UF_BALL'] < 3000:
                $new[Config::GROUP_1] = Config::GROUP_1;
                break;
            case $arUser['UF_BALL'] >= 3000 and $arUser['UF_BALL'] < 5000:
                $new[Config::GROUP_3] = Config::GROUP_3;
                break;
            case $arUser['UF_BALL'] >= 5000:
                $new[Config::GROUP_5] = Config::GROUP_5;
                break;
        }
        if ($groups != $new) {
            CUser::SetUserGroup($arFields['ID'], $new);
        }
    }

}
